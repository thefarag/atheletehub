json.extract! athelete, :id, :name, :birthdate, :created_at, :updated_at
json.url athelete_url(athelete, format: :json)
