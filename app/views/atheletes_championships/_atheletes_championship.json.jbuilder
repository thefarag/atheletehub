json.extract! atheletes_championship, :id, :athelete_id, :championship_id, :grant_date, :created_at, :updated_at
json.url atheletes_championship_url(atheletes_championship, format: :json)
