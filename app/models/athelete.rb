class Athelete < ApplicationRecord
    has_and_belongs_to_many :skills, through: :athelete_skill
    has_many :atheletes_championships
    has_and_belongs_to_many :championships, through: :atheletes_championship

    def self.search_by_name(name)
        if name.empty?
            { error: "Empty Search Query" }
        else
            { atheletes: where(["lower(name) LIKE ?","%#{name.downcase}%"]) }
        end
    end

    def self.search_by_skill(skill)
        if skill.empty?
            { error: "Empty Search Query" }
        else
            main_skills = Skill.search_by_name(skill)
            extended_skills = Skill.extend_skills_to_children(main_skills)
            { atheletes: joins(:atheletes_skills).where('atheletes_skills.skill' => extended_skills).uniq, 
              skills: main_skills, extended_skills: extended_skills}
        end
    end

    def self.search_by_age_range(age_range)
        match = age_range.match /^(\d+)\-(\d+)$/ # Age Range
        if match
            age_from = match[1].to_i
            age_to = match[2].to_i
            birthdate_from = (age_to).years.ago
            birthdate_to = (age_from).years.ago
            { atheletes: where(birthdate: birthdate_from..birthdate_to) }
        else
            match = age_range.match /^([><])(\d+)$/ # Comparator and Age
            unless match
                return { error: "Invalid Age Range (correct examples: 2-10, <6, >2)" }
            end
            age = match[2].to_i
            birthdate = (age).years.ago
            comparator = match[1] == '>' ? '<' : '>'
            { atheletes: where(["birthdate #{comparator} ?", birthdate]) }
        end
    end

    def self.search_by_experience(years_of_experience)
        unless years_of_experience =~ /^\d+$/
            return { error: "Invalid Query (correct examples: 6, 2, 11)" }
        end
        years_of_experience = years_of_experience.to_i
        first_championship_date = years_of_experience.years.ago
        { atheletes: joins(:atheletes_championships)
                    .select('atheletes.* , MIN(atheletes_championships.grant_date) AS least_grant_date')
                    .group('atheletes.id').having('MIN(atheletes_championships.grant_date) <= ?', first_championship_date) }
    end

end