class Skill < ApplicationRecord
  belongs_to :parent, class_name: 'Skill', optional: true
  has_many :children, class_name: 'Skill', foreign_key: 'parent_id'
  has_and_belongs_to_many :atheletes, through: :athelete_skill
  
  MAX_DEPTH = 5

  def name_short 
    name
  end

  def self.search_by_name(name)
      if name 
          where(["lower(name) LIKE ?","%#{name.downcase}%"])
      else
          all
      end
  end

  def self.extend_skills_to_children(skills)
    extended_skills = []
    skills.each do |skill|
      extended_skills += self.extend_skill_to_children(skill)
    end
    extended_skills.uniq
  end

  private
    def self.extend_skill_to_children(skill, depth = 0)
      return [] if depth > MAX_DEPTH # Prevent Infinite Loops if a bad skill-design was made
      extended_skills = [skill]
      skill_children = skill.children
      if skill_children
        skill_children.each do |child|
          extended_skills += self.extend_skill_to_children(child, depth + 1)
        end
      end
      extended_skills
    end

end
