class SearchController < ApplicationController
  def index
    @results = case search_params[:search_type]
    when "name"
      Athelete.search_by_name(search_params[:query])
    when "age_range"
      Athelete.search_by_age_range(search_params[:query])
    when "experience"
      Athelete.search_by_experience(search_params[:query])
    when "skill"
      Athelete.search_by_skill(search_params[:query])
    end
  end

  private
    def search_params
      params.permit(:query, :search_type)
    end
end
