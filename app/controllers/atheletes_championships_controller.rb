class AtheletesChampionshipsController < ApplicationController
  before_action :set_atheletes_championship, only: [:show, :edit, :update, :destroy]

  # GET /atheletes_championships
  # GET /atheletes_championships.json
  def index
    @atheletes_championships = AtheletesChampionship.all
  end

  # GET /atheletes_championships/1
  # GET /atheletes_championships/1.json
  def show
  end

  # GET /atheletes_championships/new
  def new
    @atheletes_championship = AtheletesChampionship.new
  end

  # GET /atheletes_championships/1/edit
  def edit
  end

  # POST /atheletes_championships
  # POST /atheletes_championships.json
  def create
    @atheletes_championship = AtheletesChampionship.new(atheletes_championship_params)

    respond_to do |format|
      if @atheletes_championship.save
        format.html { redirect_to @atheletes_championship, notice: 'Atheletes championship was successfully created.' }
        format.json { render :show, status: :created, location: @atheletes_championship }
      else
        format.html { render :new }
        format.json { render json: @atheletes_championship.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /atheletes_championships/1
  # PATCH/PUT /atheletes_championships/1.json
  def update
    respond_to do |format|
      if @atheletes_championship.update(atheletes_championship_params)
        format.html { redirect_to @atheletes_championship, notice: 'Atheletes championship was successfully updated.' }
        format.json { render :show, status: :ok, location: @atheletes_championship }
      else
        format.html { render :edit }
        format.json { render json: @atheletes_championship.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /atheletes_championships/1
  # DELETE /atheletes_championships/1.json
  def destroy
    @atheletes_championship.destroy
    respond_to do |format|
      format.html { redirect_to atheletes_championships_url, notice: 'Atheletes championship was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_atheletes_championship
      @atheletes_championship = AtheletesChampionship.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def atheletes_championship_params
      params.require(:atheletes_championship).permit(:athelete_id, :championship_id, :grant_date)
    end
end
