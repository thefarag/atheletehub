# README

In this README I'll describe the instructions, assumptions, decisions, and possible next steps.

Instructions:
    * Here is a working demo hosted at Heroku (https://athelete-hub.herokuapp.com/) which is prepopulated by the sample data
    * The links at the home page link to the datasets, so that you can easily add more Atheletes, Skills, Championships, or relations between Atheletes and Championships (Athelete Championships)
    * The search box allows multiple search according to the selected search type
    * Most of the Search logic exists in search_controller.rb, athelete.rb, skill.rb

Assumptions:
    * As this test is aimed towards coding skills, I assumed the simplest assumptions to get it done.
    * To decide the tools and frameworks to use I had some questions about the nature/frequency of these queries and the scale of such dataset, and the expected growth of it, as this is needed to understand which path to optimize for, is it scale, or performance for example, and to consider the costs of that; So I assumed low cost, small scale and not very frequent queries.
    * As it wasn't clear what is meant by "Professional experience", I assumed that this would be the years since the athelete's first championship.
    * As every championship has to have happened in a certain year, I assumed the existence of a grant_date for each relation between an Athelete and a Championship.

Desisions:
    * I choose Ruby on Rails for development as it has:
    * Quite an amazing ORM Framework (ActiveRecord)
    * A great scaffolding mechanism that helps generate basic Views for database models, which is useful for such quick proof of concepts
    * My familiarity with it for building my personal APIs on it

Next Steps:
    * Check the auto-generated Tests, and Add some new.
    * Make the search more sophisticated (to allow multiple keywords for example, and use search best practices)
    * Optimize the search performance by some normalized tables if needed (especially for the years of experience)
    * Check if any caching is needed for the upcomming scale goals
    * Add UI data verifiers for each search type to reduce the reliance on the server for that
    * And if we are moving towards a real world application, we'll need to build all the rest of basics, such as security, authentication, user roles, and good UX/UI.
