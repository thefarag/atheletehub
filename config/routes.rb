Rails.application.routes.draw do
  get :search, 'search/index'
  root 'static_pages#home'
  resources :atheletes
  resources :championships
  resources :skills
  resources :atheletes_championships
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
