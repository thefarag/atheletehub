require 'test_helper'

class AtheletesChampionshipsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @atheletes_championship = atheletes_championships(:one)
  end

  test "should get index" do
    get atheletes_championships_url
    assert_response :success
  end

  test "should get new" do
    get new_atheletes_championship_url
    assert_response :success
  end

  test "should create atheletes_championship" do
    assert_difference('AtheletesChampionship.count') do
      post atheletes_championships_url, params: { atheletes_championship: { athelete_id: @atheletes_championship.athelete_id, championship_id: @atheletes_championship.championship_id, grant_date: @atheletes_championship.grant_date } }
    end

    assert_redirected_to atheletes_championship_url(AtheletesChampionship.last)
  end

  test "should show atheletes_championship" do
    get atheletes_championship_url(@atheletes_championship)
    assert_response :success
  end

  test "should get edit" do
    get edit_atheletes_championship_url(@atheletes_championship)
    assert_response :success
  end

  test "should update atheletes_championship" do
    patch atheletes_championship_url(@atheletes_championship), params: { atheletes_championship: { athelete_id: @atheletes_championship.athelete_id, championship_id: @atheletes_championship.championship_id, grant_date: @atheletes_championship.grant_date } }
    assert_redirected_to atheletes_championship_url(@atheletes_championship)
  end

  test "should destroy atheletes_championship" do
    assert_difference('AtheletesChampionship.count', -1) do
      delete atheletes_championship_url(@atheletes_championship)
    end

    assert_redirected_to atheletes_championships_url
  end
end
