require "application_system_test_case"

class AtheletesChampionshipsTest < ApplicationSystemTestCase
  setup do
    @atheletes_championship = atheletes_championships(:one)
  end

  test "visiting the index" do
    visit atheletes_championships_url
    assert_selector "h1", text: "Atheletes Championships"
  end

  test "creating a Atheletes championship" do
    visit atheletes_championships_url
    click_on "New Atheletes Championship"

    fill_in "Athelete", with: @atheletes_championship.athelete_id
    fill_in "Championship", with: @atheletes_championship.championship_id
    fill_in "Grant date", with: @atheletes_championship.grant_date
    click_on "Create Atheletes championship"

    assert_text "Atheletes championship was successfully created"
    click_on "Back"
  end

  test "updating a Atheletes championship" do
    visit atheletes_championships_url
    click_on "Edit", match: :first

    fill_in "Athelete", with: @atheletes_championship.athelete_id
    fill_in "Championship", with: @atheletes_championship.championship_id
    fill_in "Grant date", with: @atheletes_championship.grant_date
    click_on "Update Atheletes championship"

    assert_text "Atheletes championship was successfully updated"
    click_on "Back"
  end

  test "destroying a Atheletes championship" do
    visit atheletes_championships_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Atheletes championship was successfully destroyed"
  end
end
