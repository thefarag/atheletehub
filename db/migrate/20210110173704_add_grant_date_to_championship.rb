class AddGrantDateToChampionship < ActiveRecord::Migration[6.0]
  def change
    add_column :championships, :grant_date, :date
    add_index :championships, :grant_date
  end
end
