class CreateSkills < ActiveRecord::Migration[6.0]
  def change
    create_table :skills do |t|
      t.string :name
      t.references :parent, null: true, foreign_key: { to_table: :skills }

      t.timestamps
    end
    add_index :skills, :name
  end
end
