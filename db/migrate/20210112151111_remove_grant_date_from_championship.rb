class RemoveGrantDateFromChampionship < ActiveRecord::Migration[6.0]
  def change
    remove_column :championships, :grant_date, :date
  end
end
