class CreateAtheletes < ActiveRecord::Migration[6.0]
  def change
    create_table :atheletes do |t|
      t.string :name
      t.date :birthdate

      t.timestamps
    end
    add_index :atheletes, :name
    add_index :atheletes, :birthdate
  end
end
