class AddIndexToAtheleteChampionshipGrantDate < ActiveRecord::Migration[6.0]
  def change
    add_index :atheletes_championships, :grant_date
  end
end
