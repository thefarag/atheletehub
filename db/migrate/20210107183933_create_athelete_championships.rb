class CreateAtheleteChampionships < ActiveRecord::Migration[6.0]
  def change
    create_table :atheletes_championships do |t|
      t.references :athelete, null: false, foreign_key: true
      t.references :championship, null: false, foreign_key: true

      t.timestamps
    end
  end
end
