class AddGrantDateToAtheleteChampionship < ActiveRecord::Migration[6.0]
  def change
    add_column :atheletes_championships, :grant_date, :date
  end
end
