# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_01_12_151622) do

  create_table "atheletes", force: :cascade do |t|
    t.string "name"
    t.date "birthdate"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["birthdate"], name: "index_atheletes_on_birthdate"
    t.index ["name"], name: "index_atheletes_on_name"
  end

  create_table "atheletes_championships", force: :cascade do |t|
    t.integer "athelete_id", null: false
    t.integer "championship_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.date "grant_date"
    t.index ["athelete_id"], name: "index_atheletes_championships_on_athelete_id"
    t.index ["championship_id"], name: "index_atheletes_championships_on_championship_id"
    t.index ["grant_date"], name: "index_atheletes_championships_on_grant_date"
  end

  create_table "atheletes_skills", force: :cascade do |t|
    t.integer "athelete_id", null: false
    t.integer "skill_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["athelete_id"], name: "index_atheletes_skills_on_athelete_id"
    t.index ["skill_id"], name: "index_atheletes_skills_on_skill_id"
  end

  create_table "championships", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_championships_on_name"
  end

  create_table "skills", force: :cascade do |t|
    t.string "name"
    t.integer "parent_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_skills_on_name"
    t.index ["parent_id"], name: "index_skills_on_parent_id"
  end

  add_foreign_key "atheletes_championships", "atheletes"
  add_foreign_key "atheletes_championships", "championships"
  add_foreign_key "atheletes_skills", "atheletes"
  add_foreign_key "atheletes_skills", "skills"
  add_foreign_key "skills", "skills", column: "parent_id"
end
